
var module = angular.module('ClientModule', []);

module.controller('ClientController', ["$scope", "clientServices", function ($scope, clientServices) {
//	$scope.clients = clientServices.clientList; 
//		   	 clientServices.list(function (list) {
//			 		$scope.clients = list;
//		   	 });

	   
	$scope.addClient = function () {
		clientServices.save($scope.client, function (list) {
			$scope.clients = list;
				$scope.client = '';
		});
	};
	$scope.updateClient = function (id) {
		clientServices.update($scope.client, function (list) {
			$scope.clients = list;
			$scope.client = '';
		});

	};

	$scope.sendScope = function (id) {
		clientServices.getOne(id, function (client) {
			$scope.client = clientServices.client;
		});

	};

	$scope.loadList = function () {
		clientServices.list(function (clientList) {	
			//$scope.clients = clientServices.clientList;
			
			$scope.clients = clientList;
			
		});

	};

	$scope.deleteClient = function (id) {
		clientServices.remove(id, function (list) {
			$scope.clients = list;
		});
	};


	$scope.cancel = function () {
		$scope.client = '';


	};



}]).directive('myCustomer', function () {
	return {
		templateUrl: 'client.html'
	};
});
		