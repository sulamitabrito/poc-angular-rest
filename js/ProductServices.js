

  module.factory('productServices', ["$http", function($http){  
    var service = {
      
    productList:{},
    clientList:{},
      product : {},
      
      allList:function (callback) {
        
        $http.get('http://localhost:9000/api/product/').
          success(function(data, status, headers, config) {
                         service.productList = data;
                  
                         if (callback) {
                      
                           callback(service.productList);                           
                         }
          }).
    	    error(function(data, status, headers, config) {
      
          });
        
      },
       getOne:function(id,callback) {
        $http.get('http://localhost:9000/api/client/'+id).
         success(function(data, status, headers, config) {
                         service.client = data;
                          if (callback) {
                      
                           callback( service.client);                           
                         }
                  
          }).
    	    error(function(data, status, headers, config) {
      
          }); 
        
      },
      
       filter:function(name,description,quantity,callback) {
        $http.get('http://localhost:9000/api/product/'+name+'/'+description+'/'+quantity).
         success(function(data, status, headers, config) {
                         service.product = data;
                          if (callback) {
                      
                           callback( service.product);                           
                         }
                  
          }).
    	    error(function(data, status, headers, config) {
      
          });
        
      },
      

      
      
      save:function (id,product,callback) {
            $http.post('http://localhost:9000/api/client/product/'+id, {"name":product.name,"description":product.description, "quantity":product.quantity})
          .success(function(data, status, headers, config) {
             if (callback) {
               callback(data);                           
             }
          }).
    	    error(function(data, status, headers, config) {
            console.log(status);
          });
        
      },
      update:function (id,idPrd,product,callback) {
        $http.put('http://localhost:9000/api/client/product/'+id+'/'+idPrd, {"name":product.name,"description":product.description,"quantity":product.quantity})
        .success(function(data, status, headers, config) {
          if (callback) {
             callback(data);                 
             }
            
         }).error(function(data, status, headers, config) {
            console.log(status);
         });
      },
      
      
      remove:function (id,callback) {
        $http.delete('http://localhost:9000/api/product/'+id)
        .success(function(data, status, headers, config) {
          if (callback) {
               callback(data);                           
             }
            
         }).error(function(data, status, headers, config) {
            console.log(status);
         });
      },
      
      removeProductForClient:function (id,product,callback) {
        $http.put('http://localhost:9000/api/client/product/'+id, {"name":product.name,"description":product.description, "quantity":product.quantity})
        .success(function(data, status, headers, config) {
          if (callback) {
               callback(data);                           
             }
            
         }).error(function(data, status, headers, config) {
            console.log(status);
         });
      },
      
  list:function(id,callback) {
        $http.get('http://localhost:9000/api/client/'+id).
         success(function(data, status, headers, config) {
                         service.client = data;
                          if (callback) {
                      
                           callback( service.client);                           
                         }
                  
          }).
    	    error(function(data, status, headers, config) {
      
          });
        
      },
      refresh:function(id,callback) {
        $http.get('http://localhost:9000/api/client/'+id).
         success(function(data, status, headers, config) {
                         service.client = data;
                          if (callback) {
                      
                           callback( service.client);                           
                         }
                  
          }).
    	    error(function(data, status, headers, config) {
      
          });
        
      },
    
      
      
      
    };
    
    return service;
  }]);
    
    
